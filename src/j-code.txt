

#every unexpected symbol is just ignored.

Per joint float commands:

[Letter][float];

J[joint number]:[0.00-180.00];   - rotate servo to angle 
S[joint number]:[0.01-180.00];   - Set step for SWEEP movement


Setting commands:
[Letter][integer];


R0; - Switch to absolute angle (default)
R1; - Switch to relative angle 
M[0-2];   Change movment type: 
         0=SWEEP (slower, step-by-step movement with waiting calculated delay after each small step movement), 
         1=FAST  (one single movement with waiting estimated delay  to complete movement, after sending angle to servos)
         2=NOWAIT - one single movement without a delay, just throws angle to servos.



Per joint commands:
[Letter][integer];

D[joint number] - Disables joint motor, detaches pin from pwm and grounds it. (forceless mode)
E[joint number] - Enables  joint motor, attaches pin to pwm

F[joint number] - gets feedback (in angles) from joint.



One letter commands:

[Letter];

P; - ping 