#include <jcode_parser.h>
#include <Arduino.h>
#include <string>

bool JCodeParser::is_char_in_array(char* array, int array_length, char incoming_byte)
{
    for (int i=0; i<  array_length; i++)
    {
        if (array[i] == incoming_byte)
        {
            return true;
        }
    }
  return false;
}

bool JCodeParser::is_char_ending(char incoming_byte)
{
 char commands [] = " ;\r";
 return is_char_in_array(commands, sizeof(commands), incoming_byte);
}

bool JCodeParser::is_char_backspace(char incoming_byte)
{
    return (incoming_byte == (0x08));
}

bool JCodeParser::is_char_escape(char incoming_byte)
{
    return (incoming_byte == (0x1B));
}

void JCodeParser::print_buffer()
{
    
  for (int i=0; i< input_buffer_index; i++)
    {
        Serial.print(input_buffer[i]);
        if (i==1) Serial.print(":");
    }
}

// ------------------------------------ identifying type of commands
 
bool JCodeParser::is_no_param_jcode(char incoming_byte)      ///  Just one letter  "P"
{
  char commands [] = "Pp";
  return is_char_in_array(commands, sizeof(commands), incoming_byte);
}

bool JCodeParser::is_int_param_jcode(char incoming_byte)
{
  char commands [] = "RrMmDdEeFfCc";
  return is_char_in_array(commands, sizeof(commands), incoming_byte);
}

bool JCodeParser::is_int_and_float_param_jcode(char incoming_byte)
{
  char commands [] = "JjSs";
  return is_char_in_array(commands, sizeof(commands), incoming_byte);
}
//-------------------------------------
bool JCodeParser::is_j_code_command(char incoming_byte)
{
  return ( 
           is_int_param_jcode (incoming_byte)   || 
           is_int_and_float_param_jcode (incoming_byte) || 
           is_no_param_jcode(incoming_byte)            
        );
}

bool JCodeParser::is_other_valid_stuff(char incoming_byte) //all other symbols that are not known commands but are allowed
{
  char commands [] = " :;-.";
  return is_char_in_array(commands, sizeof(commands), incoming_byte) || isdigit(incoming_byte) ||  is_j_code_command( incoming_byte );
}


bool JCodeParser::is_valid_float_symbol(char incoming_byte)
{
  char symbols [] = "-.";
  return is_char_in_array(symbols, sizeof(symbols), incoming_byte) || isdigit(incoming_byte);
}

bool JCodeParser:: validate_float_in_buffer()
{
  //Serial.println("validate_float");
  int offset=2;  //float starts here. At least should.

  for(int i=offset+1; i< input_buffer_index; i++)
  {
    if(input_buffer[i]=='-') {//Serial.print("minusfail");
    return false;};  // minus is allowed only at first position
  }

  for(int i=offset; i< input_buffer_index; i++)
  {
    if (! is_valid_float_symbol(input_buffer[i]) ) {//Serial.print("notvalidsymbol");
    return false;};  // minus is allowed only at first position
  }

  int points=0;
  for(int i=offset; i< input_buffer_index; i++)  // we should not have mor than one point
  {
     if (input_buffer[i] == '.') points++;
     if (points>1) {//Serial.print("pointstoomany");
     return false;};
  }

  if (input_buffer[input_buffer_index-1]== '.')  {//Serial.print("pointatend");      // float should not end with float
  return false;};
return true;  

}

int JCodeParser::char_to_digit(char incoming_byte)
{
  return (incoming_byte - '0');
}

int  JCodeParser::parse_buffer()
{ 
  //  expected format of the buffer:
  //  no param jcode:          "[letter]"
  // int param jcode:          "[letter][digit]"
  // int and float param jcode "[letter][digit][float number]
  // 
  // input_buffer__index points to the next char AFTER the last actually used.  (e.g. 1 for no param,   2 for int param)


  //result is stored to  parsing_results.[command, int_parameter, float parameter]

  //--------------------------------------- no params j-code
  int command = input_buffer[0];
  //look if first letter is command
  if(! is_j_code_command(command)) {return ERROR;};
  
  parsing_results.command = toupper(command);  // for this point only uppercase commands are stored.
  //if it is a command with no params, that's enough.
  if(is_no_param_jcode(command)) {return DONE;};


  //--------------------------------------- int param j-code
  //now we expect second char to be a digit.  if not, that's bad.
  if (input_buffer_index<2  || !isdigit(input_buffer[1]) ) {return ERROR;};
  
  parsing_results.int_parameter = char_to_digit(input_buffer[1]);

  // if it is only integer parameter command, that's enough
  if (is_int_param_jcode(command)) {return DONE;};

  
  //--------------------------------------- int and float  params j-code
  // now we expect the rest of the buffer being a float number.
  // it should  have third char
  if (input_buffer_index<3 ) {return ERROR;};

  // let's try converting  to float
  // using string  to reach std library conversion functions.
   
  if(!validate_float_in_buffer( )){return ERROR;};  // if we will not validate it manually, std function will crash and arduino does not have exception handling

  //filling temporary string with  the rest of the buffer data that should contain float.
  
  std::string  conversion_string;
  for (int i=2; i < input_buffer_index ; i++)
  {
    conversion_string.push_back(input_buffer[i]);
  }
  parsing_results.float_parameter = std::stof(conversion_string);
  
return DONE;
}


void  JCodeParser::debug_buffer()
{ 
  Serial.print("[");
  Serial.print(input_buffer_index);
  Serial.print("]");
    for (int i=0; i< input_buffer_index; i++)
    {
        Serial.print("|");
        Serial.print(i);
        Serial.print(":");
        Serial.print(input_buffer[i]);
    }
  Serial.print("|"); Serial.println("");
}

void JCodeParser::clean_buffer()
{
  input_buffer_index=0; 
  for (int i=0; i<sizeof(input_buffer); i++)
  {
    input_buffer[i]='*';
  }
}

ParsingResults JCodeParser::process_serial_input(char incoming_byte)   
{
    if (is_char_ending(incoming_byte)) // We recieved end of the command character
    {
        switch ( parse_buffer())
        {
            case ERROR:
                  //  Serial.print ("       Parsing error  :"); Serial.println(input_buffer[0]);
                  //  print_buffer();
                    parsing_results.parsing_status = ERROR;
                    return  parsing_results;
            case DONE:
                    Serial.println(incoming_byte); 
                    Serial.print("DOING: ");   Serial.print(parsing_results.command);  
                    if (is_int_param_jcode(parsing_results.command) || is_int_and_float_param_jcode(parsing_results.command))
                    {
                     Serial.print(" "); Serial.print(parsing_results.int_parameter);  Serial.print(" ");
                     if ( is_int_and_float_param_jcode(parsing_results.command)){
                     Serial.print(parsing_results.float_parameter);}
                    }
                     Serial.println("");
            
                    parsing_results.parsing_status = DONE;
                    //input_buffer_index=0;
                    return  parsing_results;
        }
    }
    else if (is_char_backspace(incoming_byte) and input_buffer_index>0)
    {
        input_buffer[input_buffer_index]='*'; //cleaning just in case
        input_buffer_index--;  // delete last character from the buffer
        Serial.write(0x08);   // ... and terminal
        Serial.write(' ');
        Serial.write(0x08);
        if (input_buffer_index==1)   // remove ':' symbol additionally
        {
          Serial.write(0x08);
          Serial.write(' ');
          Serial.write(0x08);
        }
        parsing_results.parsing_status = PROCESSING;
    }
    else if (is_char_escape(incoming_byte) )  // escape cancels current input and resets console
    {
        clean_buffer(); // clear buffer.
        Serial.println(" ");   // new command line
        Serial.print("> ");
        parsing_results.parsing_status = PROCESSING;

    }
    else if (incoming_byte == 'z')
    {
        debug_buffer();
    }
    else if (is_other_valid_stuff(incoming_byte) && input_buffer_index<sizeof(input_buffer))
    {   Serial.print(incoming_byte);
        input_buffer[input_buffer_index]=incoming_byte;
        input_buffer_index++;
        parsing_results.parsing_status = PROCESSING;
        
        if (input_buffer_index==2)
        {
          Serial.print(":");   // : is not part of the syntax, but it is visually comfortable to divide float value from int value
        }  
         parsing_results.parsing_status = PROCESSING; 
    }
    else 
    {
        //    we just ignore all other symbols
         parsing_results.parsing_status = PROCESSING;
    }
    return  parsing_results;
}