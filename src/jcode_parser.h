
typedef struct ParsingResults{
            char command;
            int  int_parameter;
            float float_parameter;
            int parsing_status;
            
        } ParsedCommand;

enum parsing_statuses  { PROCESSING = 0, ERROR= 1, DONE = 2 };

class JCodeParser
{
    public:
        char input_buffer[20];
        int  input_buffer_index=0;
        ParsingResults parsing_results;

        bool is_char_in_array(char* array, int array_length, char incoming_byte);
        int char_to_digit(char incoming_byte);
        
        bool is_char_ending(char incoming_byte);
        bool is_char_backspace(char incoming_byte);
        bool is_char_escape(char incoming_byte);
        bool is_other_valid_stuff(char incoming_byte);

        bool is_valid_float_symbol(char incoming_byte);
        bool validate_float_in_buffer();
    
        void print_buffer();
        int  parse_buffer();
        void clean_buffer();
        void debug_buffer();
    
        ParsingResults process_serial_input(char incoming_byte);

        bool is_no_param_jcode(char incoming_byte);
        bool is_int_param_jcode(char incoming_byte);
        bool is_int_and_float_param_jcode(char incoming_byte);
        bool is_j_code_command(char incoming_byte);
    
        
        

};
 