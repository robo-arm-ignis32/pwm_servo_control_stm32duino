
/*
void parse_float_J_value_after_decimal(char incoming_byte) //e.g  123.[546]
{

  if (isdigit(incoming_byte))
  {
    input_buffer.push_back(incoming_byte); //append char to the end of the buffer
    Serial.print((char)incoming_byte);
  }

  if (incoming_byte == ';' || (incoming_byte == ' ') || (incoming_byte == '\n'))
  {
    Serial.println((char)incoming_byte);
    Serial.println("OK");
    process_movement();
    current_parser_ptr = parse_start;
    Serial.println("");
    Serial.print("> ");
    return;
  }

  if (incoming_byte == (0x08) && !input_buffer.empty()  ) /// backspace   //TBD  j0:.;   is not covered, still can ruin.
  {

    if (input_buffer.back() == '.') // if we removed decimal delimiter, we are editing integer part again
    {
      current_parser_ptr = parse_float_J_value_before_decimal;
      return;
    }
    input_buffer.pop_back();
    Serial.write(0x08);
    Serial.write(' ');
    Serial.write(0x08);
    return;
  }

  if (incoming_byte == (0x1B)) // ESCape. Lets return to inital >___ mode.
  {
    Serial.println();
    Serial.print("> ");
    current_parser_ptr = parse_start;
    return;

  }

  return;
}

void parse_float_J_value_before_decimal(char incoming_byte) //e.g  [123.]54
{
  if (isdigit(incoming_byte) || (incoming_byte == '.')|| (incoming_byte == '-'))
  {
    input_buffer.push_back(incoming_byte); //append char to the end of the buffer
    Serial.print((char)incoming_byte);
  }

  if ((incoming_byte == '.')) //and (input_buffer !=""))
  {
    current_parser_ptr = parse_float_J_value_after_decimal;
    return;
  }

  if ((incoming_byte == ';' || (incoming_byte == ' ') || (incoming_byte == '\n')) && !input_buffer.empty()) // we allow several command terminators
  {
    Serial.println((char)incoming_byte);
    Serial.println("OK");
    process_movement();
    Serial.println("");
    Serial.print("> ");
    current_parser_ptr = parse_start;
    return;
  }

  if (incoming_byte == (0x08) && !input_buffer.empty()) /// Backspace. Delete last symbol
  {
    input_buffer.pop_back();
    Serial.write(0x08);
    Serial.write(' ');
    Serial.write(0x08);
    return;
  }

  if (incoming_byte == (0x08) && input_buffer.empty()) ///Backspace. wow, we removed all the float argument already, probably user wants to change joint number again
  {
    Serial.write(0x08);
    Serial.write(0x08);
    Serial.write(' ');
    Serial.write(' ');
    Serial.write(0x08);
    Serial.write(0x08);
    current_parser_ptr = parse_J;
    return;
  }

  if (incoming_byte == (0x1B)) // // ESCape. Lets return to inital >___ mode.
  {
    Serial.println();
    Serial.print("> ");
    current_parser_ptr = parse_start;
    return;
  }
  return;
}

void parse_J(char incoming_byte) /// e.g  J[1-6]     Now we now that we are doing J command and expect Joint number
{
  if (isdigit(incoming_byte) and incoming_byte<'0'+AMOUNT_OF_JOINTS)
  {
    input_joint_number = char_to_digit(incoming_byte);
    Serial.print((char)incoming_byte);
    Serial.print(":");
    current_parser_ptr = parse_float_J_value_before_decimal;
    return;
  }

  if (incoming_byte == (0x08)) // Backspace. Operator has changed his mind and wants another command, not J
  {
    Serial.write(0x08);
    Serial.write(' ');
    Serial.write(0x08);
    current_parser_ptr = parse_start;
    return;
  }

  if (incoming_byte == (0x1B)) // ESCape. Lets return to inital >___ mode.
  {
    Serial.println();
    Serial.print("> ");
    current_parser_ptr = parse_start;
    return;
  }

}

void parse_M(char incoming_byte) ;

void parse_confirm_M(char incoming_byte) //e.g  123.[546]
{

  
  if (incoming_byte == ';' || (incoming_byte == ' ') || (incoming_byte == '\n'))
  {
    Serial.print((char)incoming_byte);

    Serial.println("");
    Serial.println("OK");
    Serial.print("> ");
    movement_type=input_int_buffer;
    current_parser_ptr = parse_start;
    return;
    
  }

  if (incoming_byte == (0x08) ) /// backspace
  {
    current_parser_ptr = parse_M; 
     
    Serial.write(0x08);
    Serial.write(' ');
    Serial.write(0x08);
    return;
  }

  if (incoming_byte == (0x1B)) // ESCape. Lets return to inital >___ mode.
  {
    Serial.println();
    Serial.print("> ");
    current_parser_ptr = parse_start;
    return;
  }

  return;
}


void parse_M(char incoming_byte) /// e.g  J[1-6]     Now we now that we are doing J command and expect Joint number
{
  
  if (isdigit(incoming_byte) and incoming_byte <='3')
  {
    input_int_buffer =   char_to_digit(incoming_byte);
    Serial.print((char)incoming_byte);
    current_parser_ptr = parse_confirm_M;
  }

  if (incoming_byte == (0x08)) // Backspace. Operator has changed his mind and wants another command, not J
  {
    Serial.write(0x08);
    Serial.write(' ');
    Serial.write(0x08);
    current_parser_ptr = parse_start;
    return;
  }

  if (incoming_byte == (0x1B)) // ESCape. Lets return to inital >___ mode.
  {
    Serial.println();
    Serial.print("> ");
    current_parser_ptr = parse_start;
    return;
  }



  return;
}

void parse_R(char incoming_byte) ;

void parse_confirm_R(char incoming_byte) //e.g  123.[546]
{

  
  if (incoming_byte == ';' || (incoming_byte == ' ') || (incoming_byte == '\n'))
  {
    Serial.print((char)incoming_byte);

    Serial.println("");
    Serial.println("OK");
    Serial.print("> ");
    rotatation_type=input_int_buffer;
    current_parser_ptr = parse_start;
    return;
    
  }

  if (incoming_byte == (0x08) ) /// backspace
  {
    current_parser_ptr = parse_R; 
     
    Serial.write(0x08);
    Serial.write(' ');
    Serial.write(0x08);
    return;
  }

  if (incoming_byte == (0x1B)) // ESCape. Lets return to inital >___ mode.
  {
    Serial.println();
    Serial.print("> ");
    current_parser_ptr = parse_start;
    return;
  }

  return;
}


void parse_R(char incoming_byte) ///  choose rotation mode. R0- absolute, R1- relative
{
  
  if (isdigit(incoming_byte) and incoming_byte <='2')
  {
    input_int_buffer =   char_to_digit(incoming_byte);
    Serial.print((char)incoming_byte);
    current_parser_ptr = parse_confirm_R;
    return;
  }

  if (incoming_byte == (0x08)) // Backspace. Operator has changed his mind and wants another command, not J
  {
    Serial.write(0x08);
    Serial.write(' ');
    Serial.write(0x08);
    current_parser_ptr = parse_start;
    return;
  }

  if (incoming_byte == (0x1B)) // ESCape. Lets return to inital >___ mode.
  {
    Serial.println();
    Serial.print("> ");
    current_parser_ptr = parse_start;
    return;
  }

}

void parse_D(char incoming_byte);
void parse_confirm_D(char incoming_byte) //e.g  123.[546]
{

  
  if (incoming_byte == ';' || (incoming_byte == ' ') || (incoming_byte == '\n'))
  {
    Serial.print((char)incoming_byte);

    Serial.println("");
    Serial.println("OK");
    Serial.print("> ");
    
    JointsControl.DettachJointFromPwm(input_int_buffer);

 

    current_parser_ptr = parse_start;
    return;
    
  }

  if (incoming_byte == (0x08) ) /// backspace
  {
    current_parser_ptr = parse_D; 
     
    Serial.write(0x08);
    Serial.write(' ');
    Serial.write(0x08);
    return;
  }

  if (incoming_byte == (0x1B)) // ESCape. Lets return to inital >___ mode.
  {
    Serial.println();
    Serial.print("> ");
    current_parser_ptr = parse_start;
    return;
  }

  return;
}

void parse_D(char incoming_byte) ///  choose joint to disable
{
  
  if (isdigit(incoming_byte) and incoming_byte < '0' + AMOUNT_OF_JOINTS)
  {
    input_int_buffer =   char_to_digit(incoming_byte);
    Serial.print((char)incoming_byte);
    current_parser_ptr = parse_confirm_D;
    return;
  }

  if (incoming_byte == (0x08)) // Backspace. Operator has changed his mind and wants another command, not J
  {
    Serial.write(0x08);
    Serial.write(' ');
    Serial.write(0x08);
    current_parser_ptr = parse_start;
    return;
  }

  if (incoming_byte == (0x1B)) // ESCape. Lets return to inital >___ mode.
  {
    Serial.println();
    Serial.print("> ");
    current_parser_ptr = parse_start;
    return;
  }



  return;
}



void parse_S(char incoming_byte) ;
void parse_float_S_value_before_decimal(char incoming_byte);

void parse_float_S_value_after_decimal(char incoming_byte) //e.g  123.[546]
{

  if (isdigit(incoming_byte))
  {
    input_buffer.push_back(incoming_byte); //append char to the end of the buffer
    Serial.print((char)incoming_byte);
  }

  if (incoming_byte == ';' || (incoming_byte == ' ') || (incoming_byte == '\n'))
  {
    Serial.println((char)incoming_byte);
    Serial.print("OK");
    JointsControl.JointStates[input_int_buffer].sweep_step=(std::stof(input_buffer));  
    current_parser_ptr = parse_start;
    Serial.println("");
    Serial.print("> ");
    return;
  }

  if (incoming_byte == (0x08) && !input_buffer.empty()  ) /// backspace   //TBD  j0:.;   is not covered, still can ruin.
  {

    if (input_buffer.back() == '.') // if we removed decimal delimiter, we are editing integer part again
    {
      current_parser_ptr = parse_float_S_value_before_decimal;
      return;
    }
    input_buffer.pop_back();
    Serial.write(0x08);
    Serial.write(' ');
    Serial.write(0x08);
    return;
  }

  if (incoming_byte == (0x1B)) // ESCape. Lets return to inital >___ mode.
  {
    Serial.println();
    Serial.print("> ");
    current_parser_ptr = parse_start;
    return;
  }

  return;
}



void parse_float_S_value_before_decimal(char incoming_byte) //e.g  R[123.]54
{
  if (isdigit(incoming_byte) || (incoming_byte == '.')|| (incoming_byte == '-'))
  {
    input_buffer.push_back(incoming_byte); //append char to the end of the buffer
    Serial.print((char)incoming_byte);
    return;
  }

  if ((incoming_byte == '.')) //and (input_buffer !=""))
  {
    current_parser_ptr = parse_float_S_value_after_decimal;
    return;
  }

  if ((incoming_byte == ';' || (incoming_byte == ' ') || (incoming_byte == '\n')) && !input_buffer.empty()) // we allow several command terminators
  {
 
    Serial.println((char)incoming_byte);
    Serial.print("OK");
    JointsControl.JointStates[input_int_buffer].sweep_step=(std::stof(input_buffer));  
    current_parser_ptr = parse_start;
    Serial.println("");
    return;
  }

  if (incoming_byte == (0x08) && !input_buffer.empty()) /// Backspace. Delete last symbol
  {
    input_buffer.pop_back();
    Serial.write(0x08);
    Serial.write(' ');
    Serial.write(0x08);
    return;
  }

  if (incoming_byte == (0x08) && input_buffer.empty()) ///Backspace. wow, we removed all the float argument already, probably user wants to change joint number again
  {
    Serial.write(0x08);
    Serial.write(0x08);
    Serial.write(' ');
    Serial.write(' ');
    Serial.write(0x08);
    Serial.write(0x08);
    current_parser_ptr = parse_S;
    return;
  }

  if (incoming_byte == (0x1B)) // // ESCape. Lets return to inital >___ mode.
  {
    Serial.println();
    Serial.print("> ");
    current_parser_ptr = parse_start;
    return;
  }
  return;
}

void parse_S(char incoming_byte) /// e.g  S[1-6]     Now we now that we are doing S command and expect Joint number
{
  if (isdigit(incoming_byte) and incoming_byte<'0'+AMOUNT_OF_JOINTS)
  {
    input_joint_number = char_to_digit(incoming_byte);
    Serial.print((char)incoming_byte);
    Serial.print(":");
    current_parser_ptr = parse_float_S_value_before_decimal;
    return;
  }

  if (incoming_byte == (0x08)) // Backspace. Operator has changed his mind and wants another command, not J
  {
    Serial.write(0x08);
    Serial.write(' ');
    Serial.write(0x08);
    current_parser_ptr = parse_start;
    return;
  }

  if (incoming_byte == (0x1B)) // ESCape. Lets return to inital >___ mode.
  {
    Serial.println();
    Serial.print("> ");
    current_parser_ptr = parse_start;
    return;
  }

}
*/


/// e.g

/*
void parse_start(char incoming_byte) // initial state of the parser. No input yet. We except a letter now.
{
  input_int_buffer=0;
  input_buffer.clear();
  input_joint_number=99;

  switch (incoming_byte)
  {
  case 'J':  // setting joint to control
  case 'j':
    current_parser_ptr = parse_J;
    Serial.print((char)incoming_byte);
    break;
  case 'P':
  case 'p':
    Serial.print((char)incoming_byte);
    ping();
    break;

  case (0x1B): // ESCape. Lets return to inital >___ mode.
    Serial.println();
    Serial.print("> ");
    current_parser_ptr = parse_start;
    break;

  case 'M': //setting movement type
  case 'm': 
    Serial.print((char)incoming_byte);
    current_parser_ptr = parse_M;
    break;

  case 'R': //setting rotation type
  case 'r': 
    Serial.print((char)incoming_byte);
    current_parser_ptr = parse_R;
    break;
  
  case 'S': //setting sweep step angle
  case 's': 
    Serial.print((char)incoming_byte);
    current_parser_ptr = parse_S;
    break;
  

  case 'D': //disable servo
  case 'd': 
    Serial.print((char)incoming_byte);
    current_parser_ptr = parse_D;
    break;
  }

  return;
}
*/


//----------
void parse_start(char incoming_byte); //declaring, definition is below.
void parse_float_J_value_before_decimal(char incoming_byte);
void parse_J(char incoming_byte);
//-------------