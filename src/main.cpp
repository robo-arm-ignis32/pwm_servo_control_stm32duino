#include <Arduino.h>
#include "USBComposite.h"
#include <servos.h>
#include <jcode_parser.h>

ServoControl JointsControl;
JCodeParser Parser;

enum { SWEEP, FAST, NOWAIT, NOWAIT_BUT_PROVIDE_ESTIMATED_DELAY }; // movement type to
enum { ABSOLUTE,RELATIVE}; // Rotation

typedef struct Arm_Settings{
            int movement_type;
            int  rotation_type;
          } Arm_Settings;

Arm_Settings arm_settings;



void print_command_line_invitation()
{
  Serial.println(" ");
  Serial.print("> ");

}


void J_action(int joint, float angle)
{

 float input_step =  JointsControl.JointStates[joint].sweep_step;
  
 switch (arm_settings.rotation_type)
 {
  case ABSOLUTE:
        switch (arm_settings.movement_type)
        {
          case SWEEP:
              JointsControl.sweep_from_current_location(joint, angle, input_step);
              break;
          case FAST:
              JointsControl.set_joint_float_angle(joint, angle);
              break;
          case NOWAIT:
              JointsControl.set_joint_float_angle_no_wait(joint, angle);
              break;
        }
    break;
  
  case RELATIVE:

        switch (arm_settings.movement_type)
        {
          case SWEEP:
             JointsControl.sweep_relative(joint, angle, input_step);
              break;
          case FAST:
              JointsControl.rotate_relative(joint, angle);
              break;
          case NOWAIT:
              JointsControl.rotate_relative_no_wait(joint, angle);
              break;
        break;
        }
 }

}

void S_action(int joint, float step)
{
  JointsControl.JointStates[joint].sweep_step=step;  
}


void F_action(int joint)
{
  delay(100);
  Serial.print("FEEDBACK:");  Serial.println( JointsControl.feedback_to_angle(joint, JointsControl.get_feedback(joint)) );
  //JointsControl.JointStates[joint].sweep_step=step;  
}


void D_action(int joint)
{
  Serial.println("Disabling power for servos.");
  digitalWrite(SERVO_POWER_CONTROL_PIN,LOW);
  
  JointsControl.JointStates[joint].control->detach();
  pinMode(JointsControl.JointDefinitions[joint].pwm_pin, OUTPUT);
  pinMode(JointsControl.JointDefinitions[joint].pwm_pin, INPUT_PULLDOWN);

  delay(1000);
  Serial.println("Enabling power for servos back.");
  digitalWrite(SERVO_POWER_CONTROL_PIN,HIGH);



}

void E_action(int joint)
{
  Serial.println("Attaching servo back to pwm");
  pinMode(JointsControl.JointDefinitions[joint].pwm_pin, OUTPUT);
  JointsControl.JointStates[joint].control->attach(JointsControl.JointDefinitions[joint].pwm_pin,JointsControl.JointDefinitions[joint].min_mcs, JointsControl.JointDefinitions[joint].max_mcs);

}

void C_action(int joint)
{
  JointsControl.calibrate_feedback(joint);
}

 

void ping()
{
  Serial.println("");
  Serial.println("PING");
  Serial.print (" ADC0 :"); Serial.println (analogRead(PB1)); 
  Serial.print (" ADC0_avg :"); Serial.println (JointsControl.get_feedback(0)); 
  Serial.print (" vref :"); Serial.println (analogRead(PB0)); 
  Serial.print (" vref-ADC0 diff :"); Serial.println (analogRead(PB0)-analogRead(PB1)); 
    
}



void execute_command(ParsingResults pr)
{
  bool failed=false;
  switch (pr.command)  /// see j-code.txt for descriptions
  {  
        // no params
        case 'P':   //P
              ping();
              break;
        // int params 
        case 'R':  //R0 R1
              if      (pr.int_parameter==0) arm_settings.rotation_type=ABSOLUTE;
              else if (pr.int_parameter==1) arm_settings.rotation_type=RELATIVE;
              else  failed = true;
              break;
        
        case 'M':  //M0 M1
              if      (pr.int_parameter==0) arm_settings.movement_type=SWEEP;
              else if (pr.int_parameter==1) arm_settings.movement_type=RELATIVE;
              else if (pr.int_parameter==2) arm_settings.movement_type=NOWAIT;
              else  failed = true;
              break;
        
        // joint float  params
        case 'S':    //Sd:f
              if (pr.int_parameter<AMOUNT_OF_JOINTS &&  0 <= pr.float_parameter && pr.float_parameter < 360)  S_action(pr.int_parameter, pr.float_parameter);
              else failed=true;
              break;

        case 'J':    //Jd:f
              if (pr.int_parameter<AMOUNT_OF_JOINTS)  J_action(pr.int_parameter, pr.float_parameter);
              else failed=true;
              break;
        // just joint params

        case 'D':
              if (pr.int_parameter<AMOUNT_OF_JOINTS)  D_action(pr.int_parameter);
              else failed=true;
              break;

        case 'C':
              if (pr.int_parameter<AMOUNT_OF_JOINTS)  C_action(pr.int_parameter);
              else failed=true;
            break;
        case 'E':
              if (pr.int_parameter<AMOUNT_OF_JOINTS)  E_action(pr.int_parameter);
              else failed=true;
            break;
        case 'F':
              if (pr.int_parameter<AMOUNT_OF_JOINTS)  F_action(pr.int_parameter);
              else failed=true;
              break;
            break;

        default:
              Serial.println('<ERROR: No such command failure.');
  }

  if (failed)
  {
      Serial.println("ERROR: command failure");
      Serial.print("<" ); Parser.print_buffer();
      //print_command_line_invitation();
  }
  else
  {
    Parser.clean_buffer();
    Serial.println("OK" ); 
    print_command_line_invitation();
  }
  
  
}



void ReadSerial()
{
  if (Serial.available() > 0)
  {
    // read the incoming byte:
    char incoming_byte = Serial.read();
    //pass to parser
    ParsingResults parsing_results = Parser.process_serial_input(incoming_byte);
   
    if (parsing_results.parsing_status == DONE )  execute_command(parsing_results);
    
  }
}


void InitServoRelay()
{
  pinMode(SERVO_POWER_CONTROL_PIN, OUTPUT);
  digitalWrite(SERVO_POWER_CONTROL_PIN, HIGH);

}


void setup()
{
  InitServoRelay();
  Serial.begin(115200);
  delay(3000);

  Serial.println("Init  Settings..");
  arm_settings.movement_type = SWEEP;
  arm_settings.rotation_type = ABSOLUTE;
  Serial.println("Init Joints..");
  JointsControl.InitJoints();
  Serial.println("Calibrate feedback on joint 0");
  JointsControl.calibrate_feedback(0);
  delay(2000);
  Serial.print("> ");




}

void loop()
{

  // JointsControl.demo();

  ReadSerial();
  //delay(300);
}

