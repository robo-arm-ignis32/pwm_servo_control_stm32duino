 

#include <Arduino.h>
#include "USBComposite.h"
#include <Servo.h>

#include <servos.h>

//#include <iostream>
#include <algorithm>
 



JointDefinition ServoControl::JointDefinitions[] = {
                        //   name         min_mcs max_mcs deadband angle_range min_angle max_angle pwm_pin delay, feedback_pin, min_fb, max_fb

                            {"TD-8120MG", 500,   2500,   5,        180,         0,        180,      PA0,   7000,  PB1,         1660,466},
                            {"SG90",      500,   2500,   10,       180,         0,        180,      PA1,   7000,  PB1,             0,0},
                            {"MG90S",     500,   2500,   5,        180,         0,        180,      PA2,   7000,  PB1,             0,0},
                        };

JointState ServoControl::JointStates[AMOUNT_OF_JOINTS];

// mapping one range to another  but for float
float ServoControl::float_map(float x, float in_min, float in_max, float out_min, float out_max)
{
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}


float ServoControl::constrain_angle(int joint, float angle)
{
    return   constrain(angle, JointDefinitions[joint].min_angle, JointDefinitions[joint].max_angle) ; // limiting angle, we do not want to accidentaly send bigger angle than servo can do.   
}



float ServoControl::feedback_to_angle(int joint, float feedback_value)
{
    /*
    for (int angle_x_4=0; angle_x_4< 180*4; angle_x_4++  )
    {
        if (JointStates[joint].calibration_points[angle_x_4] > feedback_value)
        return  (angle_x_4/4);


    }

    return 0;
   
   // int index = int(feedback_value/4);
    ///Serial.println(index);
    //return JointStates[joint].calibration_points[index];
    

    return 0;*/
    return   map(feedback_value,JointDefinitions[joint].min_fb, JointDefinitions[joint].max_fb, JointDefinitions[joint].min_angle,JointDefinitions[joint].max_angle );
}

void ServoControl::calibrate_feedback(int joint)
{

    
    set_joint_float_angle(joint,JointDefinitions[joint].min_angle);
    delay(1000);
    JointDefinitions[joint].min_fb=get_feedback(joint);
    set_joint_float_angle(joint,JointDefinitions[joint].max_angle);
    delay(1000);
    JointDefinitions[joint].max_fb=get_feedback(joint);
    


/*
    float step=0.25;
    set_joint_float_angle(joint,0);

    for (float angle = 0; angle <= 180; angle += step) 
    { 
                set_joint_float_angle(joint,angle);
                JointStates[joint].calibration_points[int(angle*4)] = get_feedback(joint);
     }
    


    Serial.println("");
    Serial.println("Calibration data: ");

    for (int angle_x_4=0; angle_x_4< 180*4; angle_x_4++  )
    {
        Serial.print (JointStates[joint].calibration_points[angle_x_4] ); Serial.print(" ");
    

    }
    return;

    */
    Serial.println("");
    Serial.print("Calibration data for joint ["); Serial.print(joint); Serial.print("] ");
    Serial.print(JointDefinitions[joint].min_fb); Serial.print(" : "),  Serial.println(JointDefinitions[joint].max_fb);
   return;


}




int ServoControl::get_feedback(int joint)
{
#define AMOUNT_OF_READINGS 100
   // Serial.print(".");
    int reading[AMOUNT_OF_READINGS];
  
    int test;               //general purpose int
 
    int mean=0;
    int result;
    boolean done;

   int adc_pin=JointDefinitions[joint].feedback_pin;
   int vref_adc_pin=PB0;
   

   for (int j=0; j<AMOUNT_OF_READINGS; j++){

      int raw_feedback_encoder_value =  analogRead(adc_pin);
      int raw_vref_encoder_value     =  analogRead(vref_adc_pin);
      

       reading[j]=   raw_vref_encoder_value-raw_feedback_encoder_value;
    //  reading[j]=  raw_feedback_encoder_value*1000 / raw_vref_encoder_value ;
      
   // reading[j]=  map(raw_feedback_encoder_value, 600,raw_vref_encoder_value,   0,4096  ) ;
      
     // delay(3);
     delayMicroseconds(10);
    }                               // sort the readings low to high in array                                
    sort(reading, reading + AMOUNT_OF_READINGS);


    int cut=20; ///discard the $cut highest and $cut lowest readings
    for (int k = cut; k <  AMOUNT_OF_READINGS-cut; k++)
    {        
      mean += reading[k];
    }
    
    result = mean/(AMOUNT_OF_READINGS - cut - cut);                  //average useful readings

    
    return  (result);

}

// mapping desired joint angle in degrees to pwm pulse width
float ServoControl::map_angle_to_mcs(int joint, float angle)
{
    float constrained_angle=constrain_angle(joint,angle);
    if (constrained_angle != angle)
    {
        Serial.print("Target angle ");  Serial.print(angle); Serial.print("has gone beyond boundaries and is constrained to "); Serial.print(constrained_angle); 
    }
    
    int target_mcs = int (float_map(constrained_angle,  JointDefinitions[joint].min_angle,   JointDefinitions[joint].max_angle,  JointDefinitions[joint].min_mcs,  JointDefinitions[joint].max_mcs));  // map angle range to PWM pulse width mcs range
    constrain(target_mcs, JointDefinitions[joint].min_mcs, JointDefinitions[joint].max_mcs); // limiting mcs as well. Actually, it should be already fixed by limiting angle, but whatever.
    return  target_mcs;
}



// trying to calculate how much time servo will need to travel to the target mcs position.
// Currently - without load. Most probably  JointStates[].delay should be increased accordingly for actual robo arm.

int ServoControl::calculate_servo_response_delay(int joint, int target_mcs) 
{
    int travel_distance_mcs =  abs( target_mcs - JointStates[joint].current_mcs);
    float degree_to_mcs_ratio = ((JointDefinitions[joint].max_mcs - JointDefinitions[joint].min_mcs) / (JointDefinitions[joint].max_angle - JointDefinitions[joint].min_angle ));   /// 2000 / 180  ; // how many mcs in degree
    return int ( (travel_distance_mcs /  degree_to_mcs_ratio ) * JointDefinitions[joint].delay);
}

// set position in floating point degrees (using int degrees seems to be too rough, servos potentionally can do about 0.5 degrees of accuracy)
void ServoControl::set_joint_float_angle (int joint, float angle)
{
    if (joint >=  AMOUNT_OF_JOINTS)
    {
        Serial.print ("There is no such joint as "); Serial.println (joint); 
        return; // somebody has fucked up and tried to use unexisting joint, let's ignore it
    }
        
    int target_mcs = map_angle_to_mcs (joint, angle);  // convert angle to pwm pulse width
    int response_delay = calculate_servo_response_delay(joint, target_mcs); // here we calculate what pulse width we use to set servo position
    //Serial.print ("Setting joint J"); Serial.print (joint); Serial.print (" to DEGREES: "); Serial.print (angle);   Serial.print (" to MCS:  "); Serial.print (target_mcs); Serial.print (" Delay:  "); Serial.print (response_delay);


    //Serial.print ("Setting joint J"); Serial.print (joint); Serial.print (" to DEGREES: "); 
    Serial.print (angle);   Serial.print (",  ");
    Serial.print (target_mcs); Serial.print (", "); 
    //Serial.print (response_delay); Serial.print (", ");
    
    JointStates[joint].control->writeMicroseconds(target_mcs);  // send position to servo via PWM
    
    delayMicroseconds(response_delay);  //wait until servo will reach the point.  // This also means that we move only one servo per time.  
    
    
    //delay(100);
    int feedback = get_feedback(joint);
    Serial.print (feedback); Serial.print (", ");
    
    Serial.print ( feedback_to_angle(joint,feedback)); Serial.println ("");
    
    
    //TBD: probably we could control servos in parallel somehow instead of waiting each servo to finish it's movement?
    JointStates[joint].current_mcs  = target_mcs; // remember last position to be able to calculate delay in the future
    JointStates[joint].current_angle  = constrain_angle(joint, angle); 
    //Serial.println("OK");
}



void ServoControl::set_joint_float_angle_no_wait (int joint, float angle)
{
    if (joint >=  AMOUNT_OF_JOINTS)
    {
        Serial.print ("There is no such joint as "); Serial.println (joint); 
        return; // somebody has fucked up and tried to use unexisting joint, let's ignore it
    }
        
    int target_mcs = map_angle_to_mcs (joint, angle);  // convert angle to pwm pulse width
    int response_delay = 0 ;//calculate_servo_response_delay(joint, target_mcs); // here we calculate what pulse width we use to set servo position
    Serial.print ("Setting joint J"); Serial.print (joint); Serial.print (" to DEGREES: "); Serial.print (angle);   Serial.print (" to MCS:  "); Serial.print (target_mcs); Serial.print (" Delay:  "); Serial.println (response_delay);
    JointStates[joint].control->writeMicroseconds(target_mcs);  // send position to servo via PWM
   // delayMicroseconds(response_delay);  //wait until servo will reach the point.  // This also means that we move only one servo per time.  
    //TBD: probably we could control servos in parallel somehow instead of waiting each servo to finish it's movement?
    JointStates[joint].current_mcs  = target_mcs; // remember last position to be able to calculate delay in the future
    JointStates[joint].current_angle  = constrain_angle(joint, angle); 
    Serial.println("OK");
}

void ServoControl::rotate_relative (int joint, float angle)
{
    set_joint_float_angle (joint,   JointStates[joint].current_angle + angle);
}
 

void ServoControl::rotate_relative_no_wait (int joint, float angle)
{
    set_joint_float_angle_no_wait (joint,   JointStates[joint].current_angle + angle);
}
 

void ServoControl::AttachJointsToPwm()
{
    //Attach servo objects to PWM pins
    for (int j=0; j < AMOUNT_OF_JOINTS; j++)
    {
        JointStates[j].control = new Servo(); 
        JointStates[j].control->attach(JointDefinitions[j].pwm_pin,JointDefinitions[j].min_mcs, JointDefinitions[j].max_mcs);  // attaches the servo on pin to the servo object    
        if (JointStates[j].control->attached())
        {
            Serial.print("Attached servo: ");  Serial.println(j) ;
        }
        else
        {
            Serial.print("Attached servo: ");  Serial.println(j) ;
        }
    }
    return;
}

void ServoControl::AttachJointToPwm(int joint)
{
        JointStates[joint].control->attach(JointDefinitions[joint].pwm_pin,JointDefinitions[joint].min_mcs, JointDefinitions[joint].max_mcs);  // attaches the servo on pin to the servo object    
        if (JointStates[joint].control->attached())
        {
            Serial.print("Attached servo: ");  Serial.println(joint) ;
        }
        else
        {
            Serial.print("Attached servo: ");  Serial.println(joint) ;
        }
    
    return;
}

void ServoControl::DettachJointFromPwm(int joint)
{
    //Attach servo objects to PWM pins
    JointStates[joint].control->detach();
    digitalWrite(JointDefinitions[joint].pwm_pin,LOW);
        
    return;
}


void ServoControl::MoveJointsToDefaultPositions()   // put all servos to the middle position and init "current_mcs"
{
    for (int j=0; j < AMOUNT_OF_JOINTS; j++)
    {    
        JointStates[j].control->write(90);  
        JointStates[j].current_mcs = map_angle_to_mcs(j,90);
        JointStates[j].current_angle = 90;
    }
    return;
}

void ServoControl::SetDefaultSweepSteps()
{

  

      for (int j=0; j < AMOUNT_OF_JOINTS; j++)
    {    
        JointStates[j].sweep_step=(0.25);  
    }
    return;
}

void ServoControl::InitJoints()
{
    AttachJointsToPwm();
    MoveJointsToDefaultPositions();
    SetDefaultSweepSteps();
}

void ServoControl::sweep(int joint,float from, float to, float step)  /// sweep from one location to another
{
    // determine direction
    if ( from < to)
    {
        step =  abs(step);
        //make sweep
        for (float angle = from; angle <= to; angle += step) 
        { 
                set_joint_float_angle(joint,angle);
        }
    }
    else
    {
        step =  -abs(step);
        //make sweep
        for (float angle = from; angle >= to; angle += step) 
        { 
                set_joint_float_angle(joint,angle);
        }
    }

    set_joint_float_angle(joint,to); // sweep may not actualy reach final destination as last step will be beyond limit
    return;
}

 
void ServoControl::sweep_from_current_location(int joint,float to, float step)
{
    sweep(joint, JointStates[joint].current_angle, to, step );
    
    return;
}

void ServoControl::sweep_relative(int joint,float to, float step)
{
    sweep(joint, JointStates[joint].current_angle, JointStates[joint].current_angle + to, step );
    
    return;
}

void ServoControl::demo() //  demo movment of servos
{
    for (int j=0; j < AMOUNT_OF_JOINTS; j++)
    {        
        Serial.println( JointDefinitions[j].name);   
        float step = 0.25;  //step in degrees
        sweep(j, 90, 0,   step);
        sweep(j, 0,  180, step);
        sweep(j, 180, 0,  step);
        sweep(j, 0,  90,  step);
    // set_joint_float_angle(j,90);
    }
}
