 


#include <Arduino.h>
#include "USBComposite.h"
#include <Servo.h>
#define AMOUNT_OF_JOINTS  3
#define SERVO_POWER_CONTROL_PIN  PB9

//------------------------------ here we define joint configuration. Nothing dynamic is stored here, only constant values
//------------------------------ here we define joint configuration. Nothing dynamic is stored here, only constant values
typedef struct JointDefinition{
            char name[40];
            int  min_mcs;  // minimal PWM pulse width
            int  max_mcs;
            int  deadband;  // is not used yet, potentionally can be used for deadband compensation in the future.
            int  angle_range;
            int  min_angle;  
            int  max_angle;  
            int  pwm_pin;
            int  delay;  //milliseconds delay per degree 

            int feedback_pin;
            int min_fb; //analog read value  for min_angle
            int max_fb; //analog read value  for max_angle
        } JointDefintion;

//------------------------------  here we define dynamic properties of joints.
 
typedef struct JointState{
            int  current_mcs;  // last known servo position
            float  current_angle;  // last known servo position
            float sweep_step;
           // int calibration_points[180*4];

            Servo* control;    // object from Arduino library that allows sending PWM impulses to servo
            bool enabled;
        } JointState;


class ServoControl {       // The class
    
    public:             // Access specifier
 
       
        static JointDefinition JointDefinitions[AMOUNT_OF_JOINTS];
        static JointState JointStates[AMOUNT_OF_JOINTS]; 

        

    
        int  get_feedback(int joint);
        // set position in floating point degrees (using int degrees seems to be too rough, servos potentionally can do about 0.5 degrees of accuracy)
        void set_joint_float_angle (int joint, float angle);
        void set_joint_float_angle_no_wait (int joint, float angle);

        void rotate_relative (int joint, float angle);
        void rotate_relative_no_wait (int joint, float angle);


        void sweep(int joint,float from, float to, float step);
        void sweep_from_current_location(int joint, float to, float step);
        void sweep_relative(int joint,float to, float step);

        void DettachJointFromPwm(int joint);
        void AttachJointToPwm(int joint);


   // private:
        float constrain_angle(int joint, float angle);
        // mapping one range to another  but for float
        float float_map(float x, float in_min, float in_max, float out_min, float out_max);
        // mapping desired joint angle in degrees to pwm pulse width
        float map_angle_to_mcs(int joint, float angle);
        float feedback_to_angle(int joint, float feedback_value);
 
        int calculate_servo_response_delay(int joint, int target_mcs) ;
        void demo();

        void AttachJointsToPwm();
        void MoveJointsToDefaultPositions();   // put all servos to the middle position and init "current_mcs"
        void InitJoints();
        void SetDefaultSweepSteps();
        void calibrate_feedback(int joint);
        

       
        
};



 